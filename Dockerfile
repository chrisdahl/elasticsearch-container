# Latest Ubuntu Image
FROM ubuntu:16.04

MAINTAINER chris dahl <chris@petshopboys-online.com>

USER root

# Get noninteractive frontend for Debian to avoid some problems:
#    debconf: unable to initialize frontend: Dialog
ENV DEBIAN_FRONTEND noninteractive

# Set terminal to xterm
ENV TERM="xterm"

# Install necessary programs
RUN apt-get update && apt-get install -y \
	apt-utils \
	zip \
	php \
	php-xml \
	php-mbstring \
	curl \
	phpunit \
	composer \
	vim \
	default-jre \
	supervisor \
	apache2

# Install security updates and patches
RUN apt-get update -q \
    && apt-get upgrade -y \
    && apt-get dist-upgrade -y \
    && apt-get autoclean -y \
	&& apt-get autoremove -y

# Adding history search with page up and page down
RUN sed -i'' 's|# "\\e\[5~": history-search-backward|"\\e\[5~": history-search-backward|' /etc/inputrc \
	&& sed -i'' 's|# "\\e\[6~": history-search-forward|"\\e\[6~": history-search-forward|' /etc/inputrc

RUN curl -L -O https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.0.1.tar.gz \
	&& tar -xvf elasticsearch-5.0.1.tar.gz \
	&& cd elasticsearch-5.0.1/bin

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN groupadd -g 1000 elasticsearch \ 
	&& useradd elasticsearch -u 1000 -g 1000 \
	&& chmod 755 -R /elasticsearch-5.0.1 \
	&& chown -R elasticsearch:1000 /elasticsearch-5.0.1

EXPOSE 80 9200

CMD ["/usr/bin/supervisord"]
